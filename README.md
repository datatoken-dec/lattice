# LATTICE #



### What is LATTICE? ###

**LATTICE** (**L**aboratory Data **ATT**ached to **I**oT **C**onnected **E**nvironment) is a multi-band **IoT** network 
that focuses on experimental laboratory data extracted from scientific instruments or datasets.

**LATTICE** utilizes the **Streamr JS Client** to extract lab datasets and forward it to the **Ocean Marketplace** 
where **Ocean** data access tokens (datatokens) are used for buy & sell access.

**LATTICE** has the ability to connect lab devices like microscopes and spectrometers 
via traditional broadband **TCP/IP**, **WiFi**, **XG** (**5G** and beyond), and **MQTT** depending on the amount of data and the use case.